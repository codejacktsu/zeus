## Zeus - Network Usage Rate Logger

Ever wonder what your ideal internet speed is from an ISP? This tool tracks your internet usage, guiding you towards making an informed decision.

This Python script is designed to monitor and log your system's network usage rate, specifically tracking the rate of data being sent and received in real-time. It calculates and logs the amount of data transferred per second, providing insights into your network's performance. The results are logged in megabytes per second (MB/s), making it easy to understand the speed at which data is moving across your network interfaces.

## Usage

`python app.py`

`network_usage_rate.log` will be generated at root directory

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT
