import psutil
import time


def get_net_io():
    net_io = psutil.net_io_counters()
    return net_io.bytes_sent, net_io.bytes_recv

def bytes_to_mb(bytes):
    return bytes / 1024 / 1024

def get_curr_rate(interval=1):
    init_sent, init_recv = get_net_io()
    time.sleep(interval)
    curr_sent, curr_recv = get_net_io()
    sent_rate = curr_sent - init_sent
    recv_rate = curr_recv - init_recv
    return sent_rate, recv_rate

def log_network_usage_rate(log_path="./network_usage_rate.log"):
    with open(log_path, 'a') as log_file:
        while True:
            sent, recv = get_curr_rate()
            mbps_sent = bytes_to_mb(sent)
            mbps_recv = bytes_to_mb(recv)
            log_msg = f"Time: {time.strftime('%Y-%m-%d %H:%M:%S')}, " \
                    f"MB/s Sent: {mbps_sent:.2f}, MB/s Received: {mbps_recv:.2f}\n"
            if mbps_sent >= 50 or mbps_recv >= 50:
                print(log_msg)
            log_file.write(log_msg)
            log_file.flush()
