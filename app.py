from utils import log_network_usage_rate


print(f"Start Network Usage Logging")
log_network_usage_rate()
print(f"End Network Usage Logging")
